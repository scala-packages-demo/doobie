// https://index.scala-lang.org/tpolecat/doobie/doobie-core-cats
// https://index.scala-lang.org/tpolecat/doobie/doobie-postgres-cats
// https://index.scala-lang.org/tpolecat/doobie/doobie-specs2-cats

lazy val doobieVersion = "0.4.4"

import $ivy.{
    `org.tpolecat::doobie-core-cats:0.4.4`,
    `org.tpolecat::doobie-postgres-cats:0.4.4`,
    `org.tpolecat::doobie-specs2-cats:0.4.4`
}

import cats._, cats.data._, cats.implicits._
import doobie.imports._

