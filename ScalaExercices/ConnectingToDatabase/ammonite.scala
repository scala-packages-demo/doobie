// https://index.scala-lang.org/tpolecat/doobie/doobie-core
// https://index.scala-lang.org/tpolecat/doobie/doobie-hikari
// https://index.scala-lang.org/typelevel/cats/cats-core
// https://index.scala-lang.org/typelevel/cats-effect

import $ivy.{
    `org.tpolecat::doobie-core:0.9.0`,
    `org.tpolecat::doobie-hikari:0.9.0`,
    `org.typelevel::cats-core:2.1.1`,
    `org.typelevel::cats-effect:2.1.4`
}

import doobie._
import doobie.implicits._

import cats._
import cats.effect._
import cats.implicits._

import doobie.hikari._

// Resource yielding a transactor configured with a bounded connect EC and an unbounded
// transaction EC. Everything will be closed and shut down cleanly after use.
val transactor: Resource[IO, HikariTransactor[IO]] =
  for {
    ce <- ExecutionContexts.fixedThreadPool[IO](32) // our connect EC
    be <- Blocker[IO] // our blocking EC
    xa <- HikariTransactor.newHikariTransactor[IO](
      "org.h2.Driver", // driver classname
      "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", // connect URL
      "sa", // username
      "", // password
      ce, // await connection here
      be // execute JDBC operations here
    )
  } yield xa

// Inspirated by:
// Scala Exercices: Connecting to Database
// https://www.scala-exercises.org/doobie/connecting_to_database
