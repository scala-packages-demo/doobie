# doobie

Functional JDBC layer for Scala. [doobie](https://index.scala-lang.org/tpolecat/doobie)

![Cats Friendly Badge](https://typelevel.org/cats/img/cats-badge-tiny.png)
* https://libraries.io/search?q=tpolecat%2Fdoobie

# Official documentation
* *Book of Doobie* http://tpolecat.github.io/projects.html

# Unofficial documentation
* [*Using doobie; Scala with PostgreSQL vs ActiveRecord*
  ](https://thoughtbot.com/blog/some-thoughts-on-postgres)
  2019-06 Matt Sumner
* [*Scala Exercices: Connecting to Database*
  ](https://www.scala-exercises.org/doobie/connecting_to_database)
